import axios from 'axios'
export default axios.create({
    baseURL: 'http://127.0.0.1/api/v1',
    timeout: 5000,
    headers: {
        'X-Auth-Token': 'f2b6637ddf355a476918940289c0be016a4fe99e3b69c83d',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'X-Requested-With'
    }
})
