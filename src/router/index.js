import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routesWithPrefix = (prefix, routes) => {
  // if(prefix === "/auth")
  return routes.map(route => {
    route.path = `${prefix}${route.path}`
    return route
  })
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/contact-us',
    name: 'Contact',
    component: () => import(/* webpackChunkName: "ContactUs" */ '../views/Contact.vue')
  },
  ...routesWithPrefix('/about', [
    {
      path: '/vision-and-mission',
      name: 'vision_and_mission',
      component: () => import(/* webpackChunkName: "VisionAndMission" */ '../views/about/VisionComponent.vue')
    },
    {
      path: '/goals-and-values',
      name: 'goals_and_values',
      component: () => import(/* webpackChunkName: "VisionAndMission" */ '../views/about/GoalComponent.vue')
    }
  ]),
  ...routesWithPrefix('/student-affair', [
    {
      path: '/discipline-and-code-of-conduct',
      name: 'discipline_and_code_of_conduct',
      component: () => import(/* webpackChunkName: "discipline_and_code_of_conduct" */ '../views/student-affairs/Discipline.vue')
    },
    {
      path: '/rights-and-responsibilities',
      name: 'rights_and_responsibilities',
      component: () => import(/* webpackChunkName: "rights_and_responsibilities" */ '../views/student-affairs/Rights.vue')
    }
  ]),
  {
    path: '*',
    name: 'notfound',
    component: () => import(/* webpackChunkName: "NotFound" */ '../views/NotFound.vue')
  },

]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'is-active',
  routes
})


router.beforeEach((to, from, next) => {
  if (to.name) {
    store.commit('setLoading', true)
  }
  next()
})
router.afterEach(() => {
  setTimeout(() => store.commit('setLoading', false), 750)
})

export default router
