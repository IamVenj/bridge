import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "bootstrap/dist/css/bootstrap.min.css"
import axios from 'axios'
import VueAWN from "vue-awesome-notifications"
import 'vue-awesome-notifications/dist/styles/style.css';

Vue.use(VueAWN)
Vue.prototype.$axios = axios

try {
  window.Popper = require('popper.js').default
  window.$ = window.jQuery = require('jquery')
  require('bootstrap')
} catch (e) {
  console.log(e)
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
