import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false
  },
  mutations: {
    setLoading(state, payLoad) {
      state.loading = payLoad;
    }
  },
  getters: {
    getLoading(state) {
      return state.loading;
    }
  },
  actions: {
  },
  modules: {
  }
})
